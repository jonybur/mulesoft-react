import React, {
	Component
} from 'react';

import {
	Button,
	Modal
} from 'react-bootstrap';

import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap/dist/css/bootstrap-theme.css';

import logo from './logo.png';
import './App.css';

import ToDoList from './todo';
import request from 'superagent';

let root = 'https://jsonplaceholder.typicode.com';

/*

React UI on top of an API

Take any CRUD API and build a UI to let a user list, add, and delete resources from it.
A table, a button, and a modal to input a new resource should be enough.

You don’t have to build the API, it doesn’t even have to be real,
you can use something like JSON Placeholder: https://jsonplaceholder.typicode.com/ or any other you like.

You can (and should) use a tool to scaffold the application and the dev environment, like Next.js (https://learnnextjs.com/) or CRA (https://github.com/facebookincubator/create-react-app), or any other seed you’d like to use.
This exercise is about React, there's no need to mess with Webpack or Babel.

To pass the exercise you must provide a repo that we can clone, run a bunch of commands to initialise if needed (ie: npm install, npm build, npm start, etc), be able to access through the browser to this UI (ie: localhost:<PORT>) and be able to interact with the underlying API using this UI: list the resources, create and delete.

BONUS:

Let the user not only list, add and delete resources but also edit and search them. 

SUPER BONUS: 

Use a global state container, like Redux, to hold the application state instead of React’s internal state.

*/

class App extends Component {

	constructor(props) {
		super(props);

		this.state = {
			value: '',
			items: [],
			showModal: false,
			description: ''
		}
		this.startUp();
	}

	startUp = (e) => {
		let that = this;
		request
			.get(root + '/todos')
			.end(function (err, res) {
				if (err || !res.ok) {
					alert('Oh no! error');
				} else {
					that.setState({
						items: JSON.parse(JSON.stringify(res.body))
					});
				}
			});
	}

	handleDelete = (data) => {
		let that = this;
		request
			.delete(root + '/todos/' + data)
			.send(data)
			.set('X-API-Key', 'foobar')
			.set('Accept', 'application/json')
			.end(function (err, res) {
				let items = that.state.items.filter((item => {
					return item.id != data
				}))

				that.setState({
					items: items
				});
				//alert('Delete completed');
			});
	}

	handlePost = (data) => {
		let that = this;
		request
			.post(root + '/todos')
			.send(data)
			.set('X-API-Key', 'foobar')
			.set('Accept', 'application/json')
			.end(function (err, res) {
				if (err || !res.ok) {
					alert('Oh no! error');
				} else {
					let data = that.state.items.slice(0);
					data.push(JSON.parse(JSON.stringify(res.body)));
					that.setState({
						items: data
					});
					//alert('Post completed');
				}
			});
	}
	
	handleEdit = (data) => {
		let that = this;
		request
			.put(root + '/todos/' + data.id)
			.send(data)
			.set('X-API-Key', 'foobar')
			.set('Accept', 'application/json')
			.end(function (err, res) {				
				//alert('Edit completed');
			});
	}

	handleInput = (e) => {
		this.setState({
			value: e.target.value
		});
	}

	handleCreateToDo = () => {
		let todo = this.state.value;
		
		this.setState({
			showModal: true
		});
	}
	
	save = () => {
		this.setState({
			showModal: false
		});
		const title = this.state.value;

		let apiObject = {
			title: title,
			userId: 1,
			id: Date.now()
		}
		this.handlePost(apiObject);

		this.setState({
			'value': '',
			description: ''
		})
	}

	close = () => {
		this.setState({
			showModal: false
		});
	}

	deleteToDo = (e) => {
		this.handleDelete(e.target.id);
	}

	handleUpdateItem = item => {
		// copies the array
		let items = this.state.items.slice(0);
		
		// finds the item
		let i=0, len = items.length;
		for(; i<len; i++) {
			if(items[i].id===item.id){
				break;
			}
		}
		
		// edits item with new item
		items[i] = item;
		this.setState({items});
		
		// puts the item
		this.handleEdit(item);
	}

	render() {
		return (
			<div className="App">
				<div className="App-header">
					<img src={logo} className="App-logo" alt="logo" />
					<h1>MuleSoft To Do List</h1>
				</div>
				<div>
					<form className="form-control">
						<Button onClick={this.handleCreateToDo} bsStyle="primary" bsSize="large">Create</Button>
					</form>
				</div>
				<Modal show={this.state.showModal} onHide={this.close}>
					<Modal.Body>
						 Title: <input type="text" value={this.state.value} onChange={this.handleInput}/>
				   
					</Modal.Body>
					<Modal.Footer>
						<div className="row">
							<div className="col-md-6">
								<Button onClick={this.close} bsStyle="default" bsSize="large">Close</Button>
							</div>
							<div className="col-md-6">
								<Button onClick={this.save} bsStyle="primary" bsSize="large">Accept</Button>
							</div>
						</div>
					</Modal.Footer>
				</Modal>
				<div>
					<ToDoList data={this.state.items} click={this.deleteToDo} onUpdate={this.handleUpdateItem}/>
				</div>
			</div>
		);
	}

}

export default App;
