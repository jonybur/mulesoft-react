import React, { Component } from 'react';
import styles from './todo.css';

import {
	Button
} from 'react-bootstrap';

import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap/dist/css/bootstrap-theme.css';

class ToDo extends Component {
	constructor(props){
		super(props);
		this.state = {
			showEdit: false
		}
	}
	
	handleSave = e => {
		e.preventDefault();
		
		this.setState({
			showEdit: false
		})
		
		let newTitle = this.titleInputElement.value;
		
		// pisa atributo "title" en el item
		let item = Object.assign({}, this.props.item, {title: newTitle});
		this.props.onUpdate(item);
		
	}
	
	edit = e => {
		this.setState({
			showEdit: !this.state.showEdit
		})
	}
	
	render(){
		let {item, click} = this.props;
		return (
			<div>
				<h1>{item.title}</h1>
				<h2>{item.body}</h2>
				<Button id={item.id} onClick={click} bsStyle="default" bsSize="small">Delete</Button>
				<Button id={item.id} onClick={this.edit} bsStyle="default" bsSize="small">Edit</Button>
				{ this.state.showEdit && (
					<form onSubmit={this.handleSave}>
						<input defaultValue={item.title} ref={ element => this.titleInputElement = element } />
						<input type="submit" />
					</form>
				)}
		   </div>
		)
	}
}

const ToDoList =({data, click, onUpdate})=> {
	return (
		<div>
			{data.map((item, index)=> <ToDo key={index} onUpdate={onUpdate} click={click} item={item} /> )}
	 	</div>
	)
}

export default ToDoList;